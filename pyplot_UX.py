from matplotlib import pyplot as plt
from matplotlib import style
import numpy as np
from matplotlib import pyplot as mp
import pandas
import os

style.use('ggplot')

data = np.loadtxt('statistics//UX.txt', delimiter=',', skiprows=1, usecols=(0,1,2,3,4,5,6,7))
t = data[:,0]
s1 = data[:,1]
s2 = data[:,2]
s3 = data[:,3]
s4 = data[:,4]
s5 = data[:,5]
s6 = data[:,6]
s7 = data[:,7]

plt.figure(figsize=(10,7))
plt.plot(t,s1)
plt.plot(t,s2)
plt.plot(t,s3)
plt.plot(t,s4)
plt.plot(t,s5)
plt.plot(t,s6)
plt.plot(t,s7)

if not os.path.exists('graphs//UX'):
    os.makedirs('graphs//UX')

plt.figure(figsize=(10,7))
plt.plot(t,s1)
plt.title('bufferFillTime')
plt.xlabel('Time')
mp.savefig('graphs//UX//bufferFillTime.png')

plt.figure(figsize=(10,7))
plt.plot(t,s2)
plt.title('totalPT')
plt.xlabel('Time')
mp.savefig('graphs//UX/totalPT.png')

plt.figure(figsize=(10,7))
plt.plot(t,s3)
plt.title('totalDT')
plt.xlabel('Time')
mp.savefig('graphs//UX/totalDT.png')

plt.figure(figsize=(10,7))
plt.plot(t,s4)
plt.title('totalLT')
plt.xlabel('Time')
mp.savefig('graphs//UX/totalLT.png')

plt.figure(figsize=(10,7))
plt.plot(t,s5)
plt.title('bytesInBuffer')
plt.xlabel('Time')
mp.savefig('graphs//UX/bytesInBuffer.png')

plt.figure(figsize=(10,7))
plt.plot(t,s6)
plt.title('mediaPT')
plt.xlabel('Time')
mp.savefig('graphs//UX/mediaPT.png')

plt.figure(figsize=(10,7))
plt.plot(t,s7)
plt.title('mediaDT')
plt.xlabel('Time')
mp.savefig('graphs//UX/mediaDT.png')