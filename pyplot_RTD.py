from matplotlib import pyplot as plt
from matplotlib import style
import numpy as np
import os

if not os.path.exists('graphs'):
    os.makedirs('graphs')

style.use('ggplot')

t = np.loadtxt('statistics//RTD.txt', unpack = True, delimiter = ',', skiprows=1)

x = []
y = []
counter = 0
minVal = 0
maxVal  = 500
i = 0
step = 500
j = max(t, key = abs) / step + 1


while i < j:
    for line in t:
        if (minVal < line < maxVal):
            counter += 1
            
    i += 1
    y.append(counter)
    counter = 0
    x.append(maxVal)
    minVal += 500
    maxVal += 500


plt.figure(figsize=(j,j/2))        
plt.xticks(x)
plt.bar(x,y, step)
plt.title('Response Time Destribution [download chunk file]')
plt.ylabel('delta N')
plt.xlabel('Response Time')
plt.savefig('graphs//RTD.png', dpi=80)