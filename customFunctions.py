from properties import host
from threading import Lock, Thread

#---------------------------------------#
# Default Headers Manager
def default_headers(self):
     self.client.headers['Cache-Control'] = "no-cache"
     self.client.headers['Accept'] = "*/*"
     self.client.headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36"
     self.client.headers['Connection'] = "keep-alive"
     self.client.headers['Host'] = host
     self.client.headers['Pragma'] = "no-cache"
     self.client.headers['Accept-Encoding'] = "gzip, deflate, sdch"

# howTo use it^:    default_headers(self)
#---------------------------------------#

#---------------------------------------#
# Writing to file
def write_to_file(file, key, string):
    myfile = open(file, key) # open file
    myfile.write(string) # writting to the file
    myfile.close() # close file
    
# howTo use it^:    write_to_file("ATG.txt", "a", "%s, %s\n" % (startThreadTime, activeThreads_gcounter))
#---------------------------------------#

#---------------------------------------#
# Modification of common for all threads variable    
def global_counter(name, value):
    lock = Lock()
    lock.acquire()
    name += value
    lock.release()
    return name

# howTo use it^:   add it after @task():     
# def flow(self):
#     global chunkRPS_gcounter
#
# After that in some place use: chunkRPS_gcounter = global_counter(chunkRPS_gcounter, 1)
#---------------------------------------#
