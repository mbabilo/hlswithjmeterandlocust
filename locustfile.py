from locust import HttpLocust, TaskSet, task
import re
import time
from threading import Lock, Thread
from properties import *
from customFunctions import default_headers, write_to_file, global_counter
import os

if not os.path.exists('statistics'):
    os.makedirs('statistics')

write_to_file("statistics//RPS.txt", "w+", "timeGetRequest, nowRPS\n") 
write_to_file("statistics//RTD.txt", "w+", "responseTime\n")
write_to_file("statistics//UX.txt", "w+", "timeGetRequest, bufferFillTime, totalPT, totalDT, totalLT, bytesInBuffer, mediaPT, mediaDT\n")

    
startTestTime = time.time()
chunkRPS_gcounter = 0


class UserBehavior(TaskSet):
    
    @task
    
    def flow(self):
        global chunkRPS_gcounter

        # request 1 = Get original playlist
        
        getStartTime = time.time()
        request_name = 'Get original playlist'
        default_headers(self)
        response = self.client.get("/" + path + "/" + playlistName + ".m3u8", name = request_name).text
        getEndTime = time.time()
        ### calculate
        bufferFillTime = getEndTime - getStartTime
        totalDT = getEndTime - getStartTime
        totalPT = getEndTime - getStartTime
        mediaPT = 0
        mediaDT = 0
        chunkListName = re.search(r'#EXT-X-STREAM-INF:BANDWIDTH=(.+?),.*RESOLUTION=(.+?)\n(.+?)\.m3u8', response).group(3)
        
        
        if (chunkListName != "null"):
            # request 2 = Chunk list request
           
            request_name = 'Chunk list request'
            default_headers(self)
            response = self.client.get(path + "/" + chunkListName + ".m3u8", name = request_name).text
            chunkNames = re.findall(r'#EXTINF:.*,\n(.+?)\.ts.*', response)
            extinf = re.findall(r'#EXTINF:(.*),\n', response)
            i = (len(chunkNames))
            j=0
            bytesInBuffer=0
            totalLT=0
            

            while j < i:
                # request 3 download chunk file
                
                getStartTime = time.time()
                request_name = 'download chunk file'
                default_headers(self)
                response = self.client.get(path + "/" + chunkNames[j] + ".ts", name = request_name).text
                getEndTime = time.time()
                j=j+1
                ### calculate
                dB = len(response)
                dT = getEndTime - getStartTime
                mediaPT = mediaPT + float(extinf[j])
                ###
                if (bytesInBuffer < bufferSize):
                    bytesInBuffer = bytesInBuffer + dB
                    bufferFillTime = bufferFillTime + dT
                    if(totalLT > 0):
                        totalLT = totalLT + dT
                else:
                    mediaDT = mediaDT + dT
                    if (1000*mediaPT < mediaDT):
                        totalLT = totalLT + mediaDT - 1000*mediaPT
			bytesInBuffer = 0
			mediaPT = 0
			mediaDT = 0
			
                totalDT = totalDT + dT
                totalPT = totalPT + float(extinf[j])
                
                
                response_time = getEndTime - getStartTime
                timeGetRequest = getEndTime - startTestTime
                chunkRPS_gcounter = global_counter(chunkRPS_gcounter, 1)
                nowRPS = chunkRPS_gcounter / timeGetRequest
                write_to_file("statistics//RPS.txt", "a", "%s, %s \n" % (timeGetRequest, nowRPS)) # open and written text to the end of file
                write_to_file("statistics//RTD.txt", "a", "%.2f\n" % (abs(response_time * 1000))) # open and written text to the end of file        
                write_to_file("statistics//UX.txt", "a", "%s, %s, %s, %s, %.2f, %s, %s, %s\n" % (timeGetRequest, bufferFillTime, totalPT, totalDT, totalLT, bytesInBuffer, mediaPT, mediaDT)) # open and written text to the end of file
                
        totalPTLT = 1000*totalPT + totalLT
	lagTimeRatio = totalLT / totalPTLT
                
        
class WebsiteUser(HttpLocust):
    task_set = UserBehavior
    min_wait = 5000
    max_wait = 9000