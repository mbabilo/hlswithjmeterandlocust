from matplotlib import pyplot as plt
from matplotlib import style
import numpy as np
import os

if not os.path.exists('graphs'):
    os.makedirs('graphs')

style.use('ggplot')

x,y = np.loadtxt('statistics//RPS.txt', unpack = True, delimiter = ',', skiprows=1)

plt.figure(figsize=(10,7))  
plt.plot(x,y)
plt.title('RPS [download chunk file]')
plt.ylabel('requests per second (n/t)')
plt.xlabel('time (sec)')
plt.savefig('graphs//RPS.png')