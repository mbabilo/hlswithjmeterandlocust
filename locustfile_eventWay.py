from locust import HttpLocust, TaskSet, events, task, web
import re
from properties import *
from customFunctions import default_headers, write_to_file, global_counter
import time,datetime

start_test_time = time.time()
chunkRPS_gcounter = 0

class UserBehavior(TaskSet):

   @task
   def flow(self):
       global chunkRPS_gcounter
       
       # request 1 = Get original playlist
       request_name = 'Get original playlist'
       default_headers(self)
       response = self.client.get("/" + path + "/" + playlistName + ".m3u8", name=request_name).text
       chunkListName = re.search(r'#EXT-X-STREAM-INF:BANDWIDTH=(.+?),.*RESOLUTION=(.+?)\n(.+?)\.m3u8', response).group(3)

       if (chunkListName != "null"):
           # request 2 = Chunk list request
           request_name = 'Chunk list request'
           default_headers(self)
           response = self.client.get(path + "/" + chunkListName + ".m3u8", name=request_name).text
           chunkNames = re.findall(r'#EXTINF:.*,\n(.+?)\.ts.*', response)
           extinf = re.findall(r'#EXTINF:(.*),\n', response)
           i = (len(chunkNames))
           j = 0

           while j < i:
               # request 3 download chunk file
               request_name = 'download chunk file'
               default_headers(self)
               response = self.client.get(path + "/" + chunkNames[j] + ".ts", name=request_name).text
               j = j + 1
               
               
               chunkRPS_gcounter = global_counter(chunkRPS_gcounter, 1)


class MyLocust(HttpLocust):
    host = "http://127.0.0.1:8089"
    min_wait = 2000
    max_wait = 5000
    task_set = UserBehavior
    request_success_stats = [list()]
    request_fail_stats = [list()]


    def __init__(self):
        super(MyLocust, self).__init__()
        events.request_success += self.hook_request_success
        events.request_failure += self.hook_request_fail
        events.quitting += self.hook_locust_quit

    def hook_request_success(self, request_type, name, response_time, response_length):
        self.request_success_stats.append([name, request_type, response_time, response_length, time.time() - start_test_time, global_counter(chunkRPS_gcounter, 1)/(time.time() - start_test_time)])

    def hook_request_fail(self, request_type, name, response_time, exception):
        self.request_fail_stats.append([name, request_type, response_time, exception])

    def hook_locust_quit(self):
        self.save_success_stats()

    def save_success_stats(self):
        import csv
        with open('success_req_stats.csv', 'wb') as csv_file:
            writer = csv.writer(csv_file)
            for value in self.request_success_stats:
                    writer.writerow(value)